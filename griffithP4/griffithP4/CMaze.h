// Add Documentation here

#ifndef CMAZE_H
#define CMAZE_H


#include <afxwin.h>
#include <string>

using namespace std;

enum sType {WALL, EMPTY, ME, MED, MER, MEU, MEL, GOAL, CRUMB};
enum dType {UP, DOWN, LEFT, RIGHT};

class CMaze
{
	public:
        CMaze ();
        CMaze (const CMaze & other);
        ~CMaze ();
        CMaze & operator = (const CMaze & other);
		void Init (int R, int C);
        void Instructions (CFrameWnd * windowP);
        void Display (CFrameWnd * windowP);
        CRect Move (dType direction);
        void Message (CFrameWnd * windowP);
		bool finished;
    private:
		struct CMazeSquare
		{
		    CMazeSquare ();
			void Display (CDC * deviceContextP);
			CRect where;
			sType what;
		};
        void Generate (int left, int top, int right, int bottom);
        CMazeSquare ** grid;
        int numRows, numCols;
        int currentRow, currentCol;
        int endRow, endCol;
};

#endif
