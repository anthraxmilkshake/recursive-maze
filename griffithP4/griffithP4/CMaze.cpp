// This file handles most of the core functions of the maze such as the specific calculations involved in generating the walls and movement.

#include <time.h>
#include "CMaze.h"
#include <string>
#include <windows.h>
#include <mmsystem.h> 
#include "resource."
using namespace std;

CMaze::CMazeSquare::CMazeSquare ()
{
// This function will: initialize squares as "EMPTY", empty squares will be floor colored.

	what = EMPTY;
}

void CMaze::CMazeSquare::Display (CDC * deviceContextP)
{
// This function will: handle the images associated with each square, as well as the orientation of the character
// Since I'm only using images and no colors, I removed the code involved with the colors.

	// Include your choice of colors and images here

		string ImageName;
		switch (what)
		{
			case WALL:

				ImageName = "WALL_BMP";
				break;
			case EMPTY:

				ImageName = "FLOOR_BMP";
				break;
			case ME:

				ImageName = "DOWN_BMP";
				break;
			case MED:

				ImageName = "DOWN_BMP";
				break;
			case MER:

				ImageName = "RIGHT_BMP";
				break;
			case MEL:

				ImageName = "LEFT_BMP";
				break;
			case MEU:

				ImageName = "UP_BMP";
				break;
			case GOAL:

				ImageName = "ZELDA_BMP";
				break;

		}
		
		CBitmap Image;
		int res = Image.LoadBitmap(ImageName.c_str());
		CDC memDC;
		memDC.CreateCompatibleDC(deviceContextP);
		memDC.SelectObject(&Image);
		deviceContextP->TransparentBlt (where.left, where.top, where.Width(),
			where.Height(), &memDC, 0, 0, 80, 80, SRCCOPY);
}

CMaze::CMaze ()
{
// This function will: initialized all of the variables and arrays for the squares that make up the maze.

        numRows = 1;
        numCols = 1;
        grid = new CMazeSquare * [1];
        grid[0] = new CMazeSquare [1];
		finished = false;
        currentRow = 0;
        currentCol = 0;
        endRow = 0;
        endCol = 0;
}

CMaze::~CMaze ()
{
// This function will: delete all of the squares. 

	for (int r = 0; r < numRows; r++)
		delete [] grid[r];
	delete [] grid;
}

void CMaze::Init (int R, int C)
{
// This function will: Calculate the Maze and start the background music
 
        PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL, SND_ASYNC | SND_RESOURCE);
		numRows = R;
        numCols = C;
        grid = new CMazeSquare * [numRows];
        for (int r = 0; r < numRows; r++)
            grid[r] = new CMazeSquare [numCols];
		
		for(int i=0; i<numCols; i++)
			for(int j=0; j<numRows; j++)
				if(i==0 || j==0 || i==numCols-1 || j == numRows-1)
					grid[j][i].what = WALL;
		
		// Put your code for generating a maze here
		srand(unsigned int(time(NULL)));
        Generate (0,0,numCols -1, numRows -1);
		finished = false;
        currentRow = 1;
        currentCol = 1;
        grid [currentRow][currentCol].what = ME;
        endRow = numRows-2;
        endCol = numCols-2;
        grid [endRow][endCol].what = GOAL;
}

void CMaze::Instructions (CFrameWnd * windowP)
{
// This function will: display the instructions for the game

	//windowP->MessageBox ("Put your instructions here\nUse backslash and n to create\nmultiple lines.",
	//	"Instructions");
	windowP->MessageBox ("Zelda: \"Link!!! Help me!\nI'm lost in this brilliantly coded maze!\"\nUse the arrow keys to rescue Princess Zelda.",
		"Instructions");
}

void CMaze::Display (CFrameWnd * windowP)
{
// This function will: Generate and empty maze.
		
		CPaintDC dc (windowP);
		CRect rect;
		windowP->GetClientRect (&rect);
		dc.SetBkMode(TRANSPARENT);
		int sqWidth = rect.Width() / numCols;
		int sqHeight = rect.Height() / numRows;
		int ulx, uly = (rect.Height() - numRows * sqHeight) / 2;
		int r, c;
		for (r = 0; r < numRows; r++)
		{
			ulx = (rect.Width() - numCols * sqWidth) / 2;
			for (c = 0; c < numCols; c++)
			{
				grid [r][c].where = CRect (ulx, uly, ulx + sqWidth, uly + sqHeight);
				ulx += sqWidth;
			}
			uly += sqHeight;
		}
		for (r = 0; r < numRows; r++)
			for (c = 0; c < numCols; c++)
				grid [r][c].Display (&dc);
}

CRect CMaze::Move (dType direction)
{
// This function will: handle the calculations of moving the character based upon key presses. Calls the appropriate image
// based on the direction pressed.

		CRect from, to;
        switch (direction)
        {
			case LEFT:
				{
					if (grid[currentRow][currentCol-1].what==GOAL)
						finished = true;
					else if (grid[currentRow][currentCol-1].what !=WALL)
					{
						from = grid[currentRow][currentCol].where;
						to = grid[currentRow][currentCol-1].where;
						grid[currentRow][currentCol].what = EMPTY;
						grid[currentRow][currentCol-1].what = MEL;
						currentCol --;
					}
					break;
				}
			case UP: 
					if (grid[currentRow-1][currentCol].what==GOAL)
						finished = true;
					else if (grid[currentRow-1][currentCol].what !=WALL)
					{
						from = grid[currentRow][currentCol].where;
						to = grid[currentRow-1][currentCol].where;
						grid[currentRow][currentCol].what = EMPTY;
						grid[currentRow-1][currentCol].what = MEU;
						currentRow --;
					}				break;
			case RIGHT: 
					if (grid[currentRow][currentCol+1].what==GOAL)
						finished = true;
					else if (grid[currentRow][currentCol+1].what !=WALL)
					{
						from = grid[currentRow][currentCol].where;
						to = grid[currentRow][currentCol+1].where;
						grid[currentRow][currentCol].what = EMPTY;
						grid[currentRow][currentCol+1].what = MER;

						currentCol ++;
					}
				break;
			case DOWN: 
					if (grid[currentRow+1][currentCol].what==GOAL)
						finished = true;
					else if (grid[currentRow+1][currentCol].what !=WALL)
					{
						from = grid[currentRow][currentCol].where;
						to = grid[currentRow+1][currentCol].where;
						grid[currentRow][currentCol].what = EMPTY;
						grid[currentRow+1][currentCol].what = MED;
						currentRow++;
					}
				break;
        }
        return from | to;
}

void CMaze::Message (CFrameWnd * windowP)
{
// This function will: Display the win message when the player wins.

	windowP->MessageBox ("Congratulations!!!\nYou've saved Princess Zelda!\nHyrule is safe again!",
		"Winner");
}

// Replace this function with the function(s) used to generate your maze
void CMaze::Generate (int left, int top, int right, int bottom)
{
    //Exits the recursion when the space runs out
    if(right - left < 3 || bottom - top < 3)
        return;

    //Picking random values for the locations of the new walls
    int vert = ((rand() % ((right - left)-2) + (left+2))/2)*2;
    int horiz = ((rand() % ((bottom - top)-2) + (top+2))/2)*2;


    //make vertical wall
    for (int i = top; i<bottom; i++)
        grid[i][vert].what = WALL;
    //make horizontal wall
    for (int i = left; i<right; i++)
        grid[horiz][i].what = WALL;

    //This part picks 3 doors, out of 4 possibilities, to be drawn on the two intersecting walls
    int no = rand() % 4;
    if(no!=0) grid[horiz][1+ 2*((rand()% (vert-left)+(left))/2)].what = EMPTY;
    if(no!=1) grid[horiz][1+ 2*((rand()% (right-vert)+(vert))/2)].what = EMPTY;
    if(no!=2) grid[1+ 2*((rand()% (horiz-top)+ top)/2)][vert].what = EMPTY;
    if(no!=3) grid[1+ 2*((rand()% (bottom-horiz)+ horiz)/2)][vert].what = EMPTY;

    //Recursively loop through
    Generate(left,top,vert,horiz);
    Generate(vert,top,right,horiz);
    Generate(left,horiz,vert,bottom);
    Generate(vert,horiz,right,bottom);
}
