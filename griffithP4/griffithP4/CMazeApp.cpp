// This is the initial file to be called.

#include <afxwin.h>
#include "CMazeApp.h"


#pragma comment(lib, "winmm.lib")//This is a default Windows api I'm using to play sound.

BOOL CMazeApp::InitInstance ()
{
// This function will: Be the first one called, handles window updates whenever something in the window moves.

		m_pMainWnd = new CMazeWin();
		m_pMainWnd->ShowWindow (m_nCmdShow);
		m_pMainWnd->UpdateWindow ();

		return TRUE;
}

CMazeApp MazeApp;