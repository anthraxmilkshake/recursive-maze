// This file handles functions associated with the window.

#include <afxwin.h>
#include "CMazeWin.h"
#include <string>
#include <windows.h>
#include <mmsystem.h> 
#include "resource."

using namespace std;

CMazeWin::CMazeWin ()
{
// This function will: Create the window and give it the best title ever.

	Create (NULL, "Legend of Zelda: Curse of Recursion");
		myMaze.Init(21, 31);
		firstTime = true;
}

afx_msg void CMazeWin::OnPaint ()
{
// This function will: Draws a new maze upon program start and restart. Gives instructions if it's the first time.

		myMaze.Display (this);
		if (firstTime)
		{
			myMaze.Instructions(this);
			firstTime = false;
		}
}

afx_msg void CMazeWin::OnKeyDown( UINT nChar, UINT nRepCnt, UINT nFlags )
{
// This function will: handle key press events and call the appropriate functions to handle movement and redrawing, or displays an error if the wrong key was pressed.

	CRect change;
	switch (nChar)
	{
	case 37: // Left arrow key
			change = myMaze.Move(LEFT);
			InvalidateRect (change, TRUE);
			break;
	case 38: // Up arrow key
			change = myMaze.Move(UP);
			InvalidateRect (change, TRUE);
			break;
	case 39: // Right arrow key
			change = myMaze.Move(RIGHT);
			InvalidateRect (change, TRUE);
			break;
	case 40: // Down arrow key
			change = myMaze.Move(DOWN);
			InvalidateRect (change, TRUE);
			break;
	default:
		MessageBox ("Key not recognized");
	}
	if (myMaze.finished)
	{
		PlaySound(MAKEINTRESOURCE(IDR_WAVE2), NULL, SND_ASYNC | SND_RESOURCE); 
		myMaze.Message (this);
		myMaze.Init(21, 31);
		Invalidate (TRUE);
	}
}

BEGIN_MESSAGE_MAP (CMazeWin, CFrameWnd)
	ON_WM_PAINT ()
	ON_WM_KEYDOWN( )
END_MESSAGE_MAP ()